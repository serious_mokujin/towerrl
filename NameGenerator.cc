#include "NameGenerator.h"
#include <string.h>
#include "game.h"
void append(char* s, char c)
{
        int len = strlen(s);
        s[len] = c;
        s[len+1] = '\0';
}
// Returns a, e, i, o or u
int isCharVowel(char letter)
{
        if (letter == 'a' || letter == 'e' || letter == 'i' || letter == 'o' || letter == 'u')
        {
                return 1;
        }
        else
        {
                return 0;
        }
}

char ToLowCase(char c)
{
    if(c >= 101 && c<= 132)
        c -= 32;
    return c;
}
char ToUpperCase(char c)
{
    if(c >= 97 && c <= 122)
        c += 32;
    return c;
}
char getRandomVowel()
{
        int randNum = rrandom(1,4);

        switch(randNum)
        {
                case 1:
                        return 'a';
                case 2:
                        return 'e';
                case 3:
                        return 'i';
                case 4:
                        return 'o';
        }
        return 'u';
}

char getRandomConsonant()
{
        // Use the ascii values for a-z and convert to char
        char randLetter = (char) rrandom(97,122);
        while (isCharVowel(randLetter))
        {
                randLetter = (char) rrandom(97,122);
        }

        return randLetter;
}


char* getRandomName()
{
        // Seed random generator
        

        int length = rrandom(5,6);
        char* retName= new char[length];// return this string 
        
        strcpy(retName, "");
        // CVCCVC or VCCVCV
        if(rrandom(1,2) < 2)
        {
                append(retName, getRandomConsonant());
                append(retName, getRandomVowel());
                append(retName, getRandomConsonant());
                append(retName, getRandomConsonant());
                
                if (length >= 5)
                    append(retName, getRandomVowel());
                if (length >= 6) 
                    append(retName, getRandomConsonant());
        }
        else
        {
                append(retName, getRandomVowel());
                append(retName, getRandomConsonant());
                append(retName, getRandomConsonant());
                append(retName, getRandomVowel());
                if (length >= 5) {append(retName, getRandomConsonant()); }
                if (length >= 6) {append(retName, getRandomVowel()); }
        }

        return retName;
}

