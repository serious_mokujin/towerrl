/* 
 * File:   Creature.h
 * Author: Semen
 *
 * Created on 16 Апрель 2012 г., 10:14
 */

#ifndef CREATURE_H
#define	CREATURE_H
#include "CreatureClass.h"
#include "GameField.h"
#include <vector>

typedef struct { int x, y; } point;
typedef std::vector<point> los;

class Creature {
public:
//    CreatureClass _class;
    CreatureClass crClass;
    
    Creature(GameField *field);
    int GetX();
    int GetY();
    void SetX(int x);
    void SetY(int y);
    void SetPos(int x, int y);
    void SetClass(Class _class);
    bool Move(unsigned short int dir); // dir - направление движения (см. numpad)
    void Print();
    bool Falling();
    bool Caught();
    bool Update();
    
    void UseSpecialAbillity();
    void QuaffPotion();
    
    void GainLevel();
    void GainLevel(int lvl);

    void GainExp(int amount);
    void GainStr(int amount);
    void GainDex(int amount);
    void GainInt(int amount);
    
    void ReduceLevel();
    void ReduceExp(int amount);
    
    void TakeDamage(int amount);
    void Heal(int amount);  
    
    int GetDexterity();
    int GetStrength();
    int GetIntelegence();
    int GetLevel();
    int GetExp();
    int GetMaxHp();
    int GetHp();
    int GetMaxExp();
    
    void MakeMeAHero();
    bool Alive();
    void Reset();
    
    los BuildLine(int x, int y);
    bool LOSisVisible(los *line);
    bool Visible(int x, int y); 
    void SetName(char* name);
    char* GetName();
    Creature* _getCr(unsigned short int dir);
    virtual ~Creature();
private:
    
    GameField *_field;
    int _x;
    int _y;
    int _strength;
    int _dexterity;
    int _intelligence;
    int _expierence;
    int _maxexpierence;
    int _level;
    int _hitpoints;
    bool _caught;
    bool _fall;
    int _fallingHeight;
    bool _jumped;
    int _jumpStage;
    int _jumpStageCounter;
    bool _jumpLeft;
    bool _hero;
    bool _alive;
    std::vector<int> plan;
    int _regenerationCounter;
    int _potions;
    char* _name;
    //////// AI variables
    bool _moveRight;
    bool _moveDown;
    bool _moveUp;
    /////////////////////
    
    void PrintInfo();
    void aiturn();
    int CalculateDamage(Creature *cr);
    void GetPositionInDirection(int dir, int &x, int &y);
    void _getDown();
    char _get(unsigned short int dir);
    void displace(unsigned short int dir);
    bool GetLos(int x, int y, los* line);
    void cycleX(int *x);
    void cycleY(int *y);
    void checkAlive();
    void saySmth(char theme, char* name);
    void _printLOS(los *line);

    bool fallDown();
};
static void swap(los *l1, los *l2);
#endif	/* CREATURE_H */

