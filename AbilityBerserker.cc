/*
 * File:   AbilityBerserker.cc
 * Author: dvdking
 *
 * Created on April 30, 2012, 12:16 PM
 */
#include "game.h"
#include "AbilityBerserker.h"
#include "Creature.h"
#include <string.h>

AbilityBerserker::AbilityBerserker():Ability()
{
    _ended = true;
};
void AbilityBerserker::Start(int str, int dex, int Int, int x, int y, char dir, bool PrintMessages)
{
   /* if(_currentMessage == NULL)
        _currentMessage = new char[40];*/
    _elapsed = 0;
    _ended = false;
    _healBonus = 0;
    _strBonus = str / 2;
    _dexBonus = str / 4;
    _intBonus = 0;
    _duration = str / 2;
    if(PrintMessages)
	{
         attron(COLOR_PAIR(1));
         PrintMessage("You're in berserker!");
		 PrintGame();
	}
}

void AbilityBerserker::OnEnd(bool PrintMessages)
{
    if(PrintMessages)
	{
		attroff(COLOR_PAIR(1));
        PrintMessage("You feel normal");
		PrintGame();
	}
}
void AbilityBerserker::OnUpdate(bool PrintMessages)
{
    if(PrintMessages)
    {
        //strcpy(_currentMessage, "");
        if(_elapsed > _duration / 4)
                PrintMessage("The berserker is almost gone");
    }
}
AbilityBerserker::~AbilityBerserker()
{
}