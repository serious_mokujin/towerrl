/*
 * File:   Ability.h
 * Author: dvdking
 *
 * Created on April 30, 2012, 11:39 AM
 */

#ifndef ABILITY_H
#define	ABILITY_H
class Ability {
public:
    char direction;

    Ability();

    virtual void Start(int str, int dex, int Int,int x, int y, char dir, bool PrintMessages);
    void Update(bool PrintMessages);

    bool IsEnded();

    char* GetCurrentMessage();

    int GetHealBonus();
    int GetStrBonus();
    int GetDexBonus();
    int GetIntBonus();

    int GetDamageGiven();// наносимый способностью урон

    virtual ~Ability();
protected:
    bool _ended;

    int _elapsed;
    int _duration;

    int _healBonus;
    int _strBonus;
    int _dexBonus;
    int _intBonus;

    int _damageGiven;// наносимый урон способностью

    virtual void OnEnd(bool PrintMessages);
    virtual void OnUpdate(bool PrintMessages);
};

#endif	/* ABILITY_H */