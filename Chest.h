/* 
 * File:   Chest.h
 * Author: dvdking
 *
 * Created on 21 Апрель 2012 г., 12:45
 */
#include <curses.h>

#include "Creature.h"
#ifndef CHEST_H
#define	CHEST_H

enum Type
{
    HealChest,
    ExperienceChest,
    UselessChest,
    StrengthChest,
    DexterityChest,
    InteligenceChest,
    PotionChest,
    AngryChest,
    DegradationChest
};

class Chest
{
public:
    int X,Y;

    Chest(int X, int Y);
    
    void Open(Creature* creature);
    
    void Print();
    
    virtual ~Chest();
private:
    Type type;
    

};

#endif	/* CHEST_H */

