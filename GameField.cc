/*
 * File:   GameFiled.cc
 * Author: Semen
 *
 * Created on 15 Апрель 2012 г., 21:17
 */

//generator settings//////////////////////
#define MIN_PLATFORM_SIZE 6
#define MIN_PLATFORM_SIZE_FOR_LADDER 3
#define CREATURE_CHANCE 7 //чем больше, тем меньше будут создаватсья
#define CHEST_CHANCE 25     //чем больше, тем меньше будут создаватсья
//////////////////////////////////////////

#include "GameField.h"
#include "Chest.h"
#include <curses.h>
#include <stdlib.h>
#include <time.h>
#include "game.h"

GameField::GameField(int width, int height)
{
    _width = width;
    _height = height;
    _field = new char*[_height];
    for(int i = 0; i < _height; i++)
        *(_field+i) = new char[_width];
    int x, y;

    _scroll = 0;

    srand(time(0));
    StartGeneration();
}
void GameField::Print()
{
    int x, y;
    for(y = 0; y < _height; y++)
    {
        move(y, 0);
        for(x = 0; x < _width; x++)
        {
            printw("%c",*(*(_field+y)+x));// printw("%c", *(*(_field+y)+( (x+_scroll)%_width )));
        }
    }
}
void GameField::ScrollLeft()
{
    if(_scroll <= 0)
        _scroll = _width - 1;
    else
        _scroll -= 1;
}
void GameField::ScrollRight()
{
    if(_scroll >= _width - 1)
        _scroll = 0;
    else
        _scroll += 1;
}

void GameField::StartGeneration()
{
    short y;
    //  генерация "пола"
    for(y = 0; y <_width; y++)
        *(*(_field + _height - 1) + y) = '=';

    for(y = _height - 2; y >= 0; y--)
        GenerateLine(y, true);
}
/*
 * Монстра вставлять с помощью функции NewCreature(x,y,level)
 * я тут хз где можно такое прописать -_.
 *
 *                                      // Денис
 * PS:  блин, прокомментеный код.. надо бы и мне тоже как нибудь перед
 *      релизом прокомментить все
 */
void GameField::GenerateLine(short line, bool firstGen)
{
    int crLevel = 0;
    int i,j, pl_size, k;
    int pl_start, pl_end;
    int pos;
    int ladderCount = 0;
    for(i = 0; i< _width; i++)
        *(*(_field + line) + i) = '.';
    while(ladderCount == 0)
    {
        for(i = 0; i< _width; i++)
        {
            if(*(*(_field + line + 1) + i) == '#')
            {
                if(i > 0)
                    if(*(*(_field + line + 1) + i - 1) == '=' ) continue;
                if(i < _width)
                    if (*(*(_field + line + 1) + i + 1) == '=')  continue;

                if(!(rand() % 2))
                {
                    // Продолжаем лестницу
                    *(*(_field + line) + i) = '#';
                    ladderCount++;
                    continue;
                }
                else
                {
                    ////////Создаем платформу///////////

                    //находим левую границу
                    j = i;
                    while(j >= 0
                            && (*(*(_field + line) + j) != '=' || *(*(_field + line) + j) != '#')
                            && *(*(_field + line + 1) + j) != '=')
                    {
                        j--;
                    //  if(j <= 0) break;
                    }
                    pl_start = j;

                    //находим правую границу
                    j = i;
                    while(j < _width
                            && (*(*(_field + line) + j) != '=' || *(*(_field + line) + j) != '#')
                            && *(*(_field + line + 1) + j) != '=')
                    {
                        j++;
                    }

                    pl_end = j;

                    // находим размеры платформы

                    pl_start = pl_start + rand() % (i- pl_start);
                    pl_end   = pl_end   - rand() % (pl_end - i);

                    // если платформа сильно маленькая, то создаем лестницу
                    if(pl_end - pl_start < MIN_PLATFORM_SIZE)
                    {
                        *(*(_field + line) + i) = '#';
                        ladderCount++;
                        continue;
                    }

                    //
                    for(i = pl_start;i< pl_end; i++)
                    {
                        if(*(*(_field + line + 1) + i) == '#')
                            *(*(_field + line) + i) = '#';
                        else
                            *(*(_field + line) + i) = '=';
                    }

                    // если что разбиваем сильно большую платформу на кусочки
                    if(pl_end - pl_start >= 7)
                    {
                        if(!(rand() % 10)) continue;//порой все же оставляем

                        pos = pl_start + rand() %(pl_end - pl_start);
                        if(*(*(_field + line) + pos) != '#')
                            *(*(_field + line) + pos) = '.';
                    }
                }
            }
            else if(*(*(_field + line + 1) + i) == '=')
            {
                //создаем новую лесенку
                pl_start = i;
                while(*(*(_field + line + 1) + i) == '=')
                {
                    pl_end = ++i;
                    if(i > _width) break;
                }
                k = 0;
                while(((!(rand() % 5) && ladderCount < 2))  && pl_end - pl_start >= 2)
                {
                    pos = pl_start + rand() %(pl_end - pl_start);// находим позицию вставки

                    if(pos - 1 < 0) pos = 0;
                    if(pos + 1 > _width) pos = _width - 1;

                    if(!(*(*(_field + line) + pos - 1) == '#' ||*(*(_field + line) + pos + 1) == '#'))
                        if(*(*(_field + line + 1) + pos) != '#')
                        {
                            ladderCount++;
                            *(*(_field + line) + pos) = '#';
                        }
                }
            }
        }
    }

    // add creatures
    if(!firstGen)
    {
        for(i = 0; i < _width; i++)
        {
            if(*(*(_field + line + 1) + i) == '=' && *(*(_field + line) + i) == '.')
                if(!(rand() % CREATURE_CHANCE))
                {
                    crLevel = hero->GetLevel();
                    crLevel += rand() % 2 - rand() % 2 + 7;
                    if(crLevel == 0) crLevel++;
                    NewCreature(i, line - 1,crLevel);
                }
        }
        for(i = 0; i < _width; i++)
            if(*(*(_field + line + 1) + i) == '=' && *(*(_field + line) + i) == '.')
            {
                if(!(rand() % CHEST_CHANCE))
                    NewChest(i, line - 1);
            }
    }
}

void GameField::ShiftUp()
{
    int i;
    char *tmp = *(_field+_height-1);
    for(i = _height-1; i>0; i--)
    {
        *(_field+i) = *(_field+i-1);
    }
    *(_field) = tmp;
    GenerateLine(0, false);
}

void GameField::DestroyTile(int x, int y)
{
    if(x < _width && y <_height && x >= 0 && y >= 0)
        *(*(_field+y)+x) = '.';
}

char GameField::GetTile(int x, int y)
{
    return *(*(_field+y)+x);
}
int GameField::GetWidth()
{
    return _width;
}
int GameField::GetHeight()
{
    return _height;
}
GameField::~GameField()
{
    /*for(int i = 0; i < _height; i++)
        delete[] *(_field + i);
    delete[] _field;*/
}