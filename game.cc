#include "game.h"
#include "curses.h"

#include <Windows.h>
//#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <iostream>

GameField *field;
Creature *hero;
std::list<Creature*> creatures;
std::list<Chest*> chests;
std::list<MagicDart*> magicDarts;

int heightExplored = 0;
// for the score table //
int monstersKilled = 0;//
int chestsOpened   = 0;//
int damageTaken    = 0;//
int damageGiven    = 0;//
int healTaken      = 0;//
/////////////////////////

char PlayerName[20];

void PrintMessage(const char* message, bool printDate)
{
    shiftBuffer();
    time_t rawtime = time(NULL);
    tm *time = localtime(&rawtime);
    //char b[12];

	if(printDate)
	{
		strftime(*_mbuffer, 11, "[%H:%M:%S]", time);
		strcpy((*_mbuffer)+10," ");
	}
	else
	{
		strcpy(*_mbuffer, "           ");
	}
	strncpy((*_mbuffer)+11, message, BUFFER_WIDTH - 1 - 11);
    *((*_mbuffer) + BUFFER_WIDTH - 1) = '\0';
    printBuffer();
}
void printBuffer()
{
    int cx;
    for(int i = BUFFER_SIZZE - 1; i >= 0; i--)
    {
        move(field->GetHeight() - i, field->GetWidth() + 2);
        printw("%s", *(_mbuffer + i));
        cx = getcurx(stdscr);
        while(cx <= field->GetWidth() + 2 + BUFFER_WIDTH)
        {
            printw(" ");
            cx = getcurx(stdscr);
        }
    }
    refresh();
}
void shiftBuffer()
{
    char *tmp = *(_mbuffer + BUFFER_SIZZE - 1);
    for(int i = BUFFER_SIZZE - 1; i > 0; i--)
    {
        *(_mbuffer + i) = *(_mbuffer + i - 1);
    }
    *(_mbuffer) = tmp;
}
void InitializeGame()
{
    creatures.clear();
    field = new GameField(19,19);
    hero = new Creature(field);
    _mbuffer = new char*[BUFFER_SIZZE];
    for(int i = 0; i < BUFFER_SIZZE; i++)
    {
        *(_mbuffer+i) = new char[BUFFER_WIDTH];
        *(*(_mbuffer+i)) = '\0';
    }
    srand(time(0));
}
Creature* GetCreature(int x, int y)
{
    for(std::list<Creature*>::iterator i = creatures.begin(); i != creatures.end(); ++i)
        if(x == (*i)->GetX() && y == (*i)->GetY())
            return *i;
    return NULL;
}
int rrandom(int min, int max)
{
    return min + rand()% (max - min);
}
void GenerateNewGame()
{
    DeleteCreatures();
    DeleteChests();
    field->StartGeneration();
    hero->MakeMeAHero();
    hero->Reset();
    hero->SetPos(10,17);
    heightExplored = 0;
    NewCreature(5,5,1);
}
void DisposeGame()
{
    delete hero;
    delete field;
    DeleteCreatures();
    DeleteChests();
}
void NewCreature(int x, int y, int lvl)
{
    Creature *cr = new Creature(field);
    cr->SetPos(x,y);
    cr->SetName(getRandomName());
    cr->GainLevel(lvl);
    creatures.push_back(cr);
}
void KillCreature(Creature *cr)
{
    for(std::list<Creature*>::iterator i = creatures.begin(); i != creatures.end(); ++i)
        if((*i) == cr)
        {
            delete (*i);
            creatures.erase(i);
            return;
        }
}
void PrintCreatures()
{
    for(std::list<Creature*>::iterator i = creatures.begin(); i != creatures.end(); ++i)
        (*i)->Print();
    hero->Print();
}
void DeleteCreatures()
{
    for(std::list<Creature*>::iterator i = creatures.begin(); i != creatures.end(); ++i)
        delete (*i);
    creatures.clear();
}
void NewChest(int x, int y)
{
    Chest *chest = new Chest(x, y);
    chests.push_back(chest);
}
void PrintChests()
{
    for(std::list<Chest*>::iterator i = chests.begin(); i != chests.end(); ++i)
        (*i)->Print();
    refresh();
}
void DeleteChests()
{
    for(std::list<Chest*>::iterator i = chests.begin(); i != chests.end(); ++i)
        delete (*i);
    chests.clear();
}
void Shift()
{
    field->ShiftUp();
    hero->SetY(hero->GetY()+1);

    for(std::list<Creature*>::iterator i = creatures.begin(); i != creatures.end(); ++i)
    {
        (*i)->SetY((*i)->GetY()+1);
        if((*i)->GetY() >= field->GetHeight())
        {
            delete (*i);
            i = creatures.erase(i);
        }
    }
    heightExplored++;
    for(std::list<Chest*>::iterator i = chests.begin(); i != chests.end(); ++i)
        {
        (*i)->Y++;
        if((*i)->Y >= field->GetHeight())
            {
            delete (*i);
            i = chests.erase(i);
            }
        }
    for(std::list<MagicDart*>::iterator k = magicDarts.begin(); k != magicDarts.end(); ++k)
    {
        (*k)->ShiftUp();
    }
}

void NewMagicDart(int x, int y, char dir, int damage)
{
    MagicDart* m = new MagicDart(x,y,dir, damage);
    magicDarts.push_back(m);
}
void UpdateMagicDarts()
{
	std::list<MagicDart*>::iterator i = magicDarts.begin();
	while (i != magicDarts.end())
	{
        if((*i)->Update() == -1)
        {
            delete (*i);
            i = magicDarts.erase(i);
        }
		else
		{
			++i;
		}
    }
}

void PrintsMagicDarts()
{
    for(std::list<MagicDart*>::iterator i = magicDarts.begin(); i != magicDarts.end(); ++i)
    {
        (*i)->Print();
    }
}

int CalculateScore()
{
   return heightExplored * 2 + hero->GetLevel()*3 + hero->GetExp() + hero->GetMaxHp();
}

void AITurn()
{
	std::list<Creature*>::iterator i = creatures.begin();
	while (i != creatures.end())
	{
		(*i)->Update();
		if((*i)->GetY() >= field->GetHeight())
		{
			i = creatures.erase(i);  // alternatively, i = items.erase(i);
		}
		else
		{
			++i;
		}
		PrintGame();
	}
    /*for(std::list<Creature*>::iterator i = creatures.begin(); i != creatures.end(); ++i)
    {
        (*i)->Update();
        if((*i)->GetY() >= field->GetHeight())
        {
            delete (*i);
            i = creatures.erase(i);
        }
        PrintGame();
    }*/
}
void PrintGame()
{
    field->Print();
    PrintChests();
    PrintCreatures();
    PrintsMagicDarts();
    refresh();
}
void GameTurn()
{
    bool res;
    res = hero->Update();
    hero->Print();
    if(!hero->Alive()) return;
    if(!res) // если во время обновления не был сделан ход (обычно это падение)
    {
        flushinp();
        switch(getch())
        {
            case KEY_LEFT:
            case 52:
                res = hero->Move(4);
                break;
            case KEY_RIGHT:
            case 54:
                res = hero->Move(6);
                break;
            case KEY_UP:
            case 56:
                res = hero->Move(8);
                break;
            case KEY_DOWN:
            case 50:
                res = hero->Move(2);
                break;
            case 49:
                res = hero->Move(1);
                break;
            case 55:
                res = hero->Move(7);
                break;
            case 57:
                res = hero->Move(9);
                break;
            case 51:
                res = hero->Move(3);
                break;
            case 53:
                res = hero->Move(5);
                break;
            case 'o':
                res = true;
                for(std::list<Chest*>::iterator i = chests.begin(); i != chests.end(); ++i)
                        if((*i)->Y == hero->GetY() && (*i)->X == hero->GetX())
                        {
                            (*i)->Open(hero);
                            delete (*i);
                            i = chests.erase(i);
                            chestsOpened++;
                            break;
                        }
                break;
            case 'q':
                hero->QuaffPotion();
                break;
            case 'a':
                res = hero->crClass.UseSpecialAbility(hero->GetStrength(),
                                                      hero->GetDexterity(),
                                                      hero->GetIntelegence(),
                                                      hero->GetX(),
                                                      hero->GetY(),
                                                      true);

                break;
        }
        while(hero->GetY()<=9)
        {
            Shift();
            PrintGame();
        }
        if(res)
        {
            hero->crClass.UpdateAbilities(true);
            UpdateMagicDarts();
            PrintGame();
            AITurn();
        }
    }
    else
    {
        Sleep(500);
        //usleep(500000);
        PrintGame();
        AITurn();
    }
    //clear();
    PrintGame();
///////////////////
//    for(std::list<Creature*>::iterator i = creatures.begin(); i != creatures.end(); ++i)
//        hero->Visible((*i)->GetX(), (*i)->GetY());
//    hero->Print();
//    refresh();
///////////////////
}