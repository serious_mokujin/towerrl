/* 
 * File:   AbilityBerserker.h
 * Author: dvdking
 *
 * Created on April 30, 2012, 12:16 PM
 */


#ifndef ABILITYBERSERKER_H
#define	ABILITYBERSERKER_H

#include "Ability.h"
class AbilityBerserker:public Ability
{
public:
    AbilityBerserker();
    void Start(int str, int dex, int Int, int x, int y, char dir, bool PrintMessages);
    //void Update(bool PrintMessages);

//    bool IsEnded();
//    
//    int GetDefBonus();
//    int GetHealBonus();
//    int GetStrBonus();
//    int GetDexBonus();
//    int GetIntBonus();
//    int GetDamageBonus();
//    int GetDamageGiven();
    
    
    virtual ~AbilityBerserker();
private:
    void OnEnd(bool PrintMessages);
    void OnUpdate(bool PrintMessages);
};

#endif	/* ABILITYBERSERKER_H */

