#include "MagicDartAbility.h"
#include "game.h"

MagicDartAbility::MagicDartAbility()
{
    _ended = true;
    direction = -1;
}

void MagicDartAbility::Start(int str, int dex, int Int,int x, int y, char dir, bool PrintMessages)
{
    _crX = x;
    _crY = y;
    direction = dir;
    _elapsed = 0;
    _ended = false;
    _healBonus = 0;
    _strBonus = 0;
    _dexBonus = 0;
    _intBonus = 0;
    _duration = 2;
    _damageGiven = Int - (Int / 4);
    NewMagicDart(_crX, _crY, direction, _damageGiven);
    if(PrintMessages)
        PrintMessage("You cast a spell of magic darts");
}

MagicDartAbility::~MagicDartAbility()
{
}