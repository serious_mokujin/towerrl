/*
 * File:   ScoreTable.cc
 * Author: dvdking
 *
 * Created on 19 Апрель 2012 г., 20:08
 */
#include <iostream>
#include <fstream>
//#include <bits/stl_list.h>
#include "ScoreTable.h"
#include "curses.h"
#include <string.h>
using namespace std;

ScoreTable::ScoreTable()
{
}

bool compare_crps (corpse first, corpse second)
{
    return first.score > second.score;
}

void ScoreTable::AddScore(corpse crps)
{
    corpses.push_back(crps);
    corpses.sort(compare_crps);
    lastCorpse = crps;
}

void ScoreTable::LoadTable()
{
    FILE *fp;
    fp = fopen(SCORES_PATH, "r");
    corpse temp;
    corpses.clear();

    if(fp != NULL)
    {
        while(!feof(fp))
        {
            fscanf(fp, "%s", &temp.name);
            fscanf(fp, "%d", &temp.score);
            corpses.push_back(temp);
        }
		fclose(fp);
    }

    corpses.sort(compare_crps);
}
void ScoreTable::SaveTable()
{
    FILE *fp;
    fp = fopen(SCORES_PATH, "w");
    corpses.sort(compare_crps);

    if(fp != NULL)
    {
        for (std::list<corpse>::iterator i = corpses.begin(); i != corpses.end(); ++i)
        {
            fprintf(fp, "%s %d", (*i).name, (*i).score);
        }
    }
    fclose(fp);
}
void ScoreTable::PrintTable()
{
    for (std::list<corpse>::iterator i = corpses.begin(); i != corpses.end(); ++i)
    {
        printw("%s\t%d\n", (*i).name, (*i).score);
    }
    refresh();
}

void ScoreTable::PrintTable(int start, int count)
{
    int k = 0; int len;
    char tab[20];

    std::list<corpse>::iterator i = corpses.begin();

    if(start != 0)
        while( i++ != corpses.end() && k++ < start);// костыль: изменяем начальную позицию в списке

    int maxL = 0, cur = 0;

    // нахождение максимального размера имени
    for (std::list<corpse>::iterator j = corpses.begin(); j != corpses.end(); j++)
    {
        cur = strlen(j->name);
        if(cur > maxL) maxL = cur;
    }

    for (;i != corpses.end() && k < start + count; i++, k++)
    {
        if(strcmp(i->name, lastCorpse.name) == 0 && i->score == lastCorpse.score)
        {
            attroff(COLOR_PAIR(1));
            attron(COLOR_PAIR(2));
        }
        else
        {
            attroff(COLOR_PAIR(2));
            attron(COLOR_PAIR(1));
        }

        strcpy(tab, "");
        len = maxL - strlen(i->name);
        while(len > 0)
        {
            len -= 1;
            strcat(tab, " ");
        }
        printw("%s%s\t%d\n", (*i).name,tab,(*i).score);
    }
}

int ScoreTable::GetCorpsesCount()
{
    return corpses.size();
}

ScoreTable::~ScoreTable()
{
}