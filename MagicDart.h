/* 
 * File:   MagicDart.h
 * Author: dvdking
 *
 * Created on May 1, 2012, 8:50 PM
 */

#ifndef MAGICDART_H
#define	MAGICDART_H

class MagicDart{
public:
    MagicDart(int x, int y, char dir, int damage);
    
    int Update();
    
    void Print();
    
    void ShiftUp();
    
    int GetX();
    int GetY();
    int GetDirection();
    int GetDamage();
    
    virtual ~MagicDart();
private:
   int _x,_y;
   char _direction;
   int _damage;
   
   void GetPositionInDirection(char dir, int &x, int &y);
};
#endif	/* MAGICDART_H */

