/*
 * File:   CreatureClass.cc
 * Author: dvdking
 *
 * Created on April 30, 2012, 11:26 AM
 */
#include "CreatureClass.h"
#include "game.h"
#include <string.h>
#include <curses.h>
#include "AbilityCrush.h"
#include "AbilityBerserker.h"
#include "MagicDartAbility.h"

CreatureClass::CreatureClass()
{
}
void CreatureClass::UpdateAbilities(bool PrintMessages)
{
    int k = executioningAbilities.size();
    if(k)
	{
		std::list<Ability*>::iterator i = executioningAbilities.begin();
		while (i != executioningAbilities.end())
		{
			(*i)->Update(PrintMessages);
			if((*i)->IsEnded())
			{
				i = executioningAbilities.erase(i);  // alternatively, i = items.erase(i);
			}
                        else
                        {
                            ++i;
                        }
		}

        /*for(std::list<Ability*>::iterator i = executioningAbilities.begin(); i != executioningAbilities.end(); i++)
        {
            (*i)->Update(PrintMessages);
            if((*i)->IsEnded())
            {
                i = executioningAbilities.erase(i);
            }
        }*/
	}
}
int CreatureClass::UseSpecialAbility(int str, int dex, int Int, int x, int y, bool PrintMessages)
{
    char k;
    char dir = 0;
    if(PrintMessages)
        PrintMessage("Choose the skill (1 - 9)");
    k = GetNumberFromKeyboard();
    int t = 0;
    for(std::list<Ability*>::iterator i = abilities.begin(); i != abilities.end() && t<9; ++i, t++)
        if(t == k - '0' - 1)
        {
            if((*i)->IsEnded())// если все нормально, то мы начинаем использование способности
            {
                if((*i)-> direction != 0)
                {
                   if(PrintMessages)
                        PrintMessage("Choose the direction (1 - 9)");
                   dir = GetNumberFromKeyboard();
                }
                (*i)->Start(str, dex, Int,x ,y , dir, PrintMessages);
                executioningAbilities.push_back((*i));
                return 1;
            }

           PrintMessage("You can't use that for a while");// если данная способность сейчас используется
            return 0;
            break;
        }
    // если по данной цифре нет способностей
    if(t == 9)
    {
          PrintMessage("There is no such skill");
                  return 0;
    }
}
void CreatureClass::SetClass(Class _class, GameField* gf)
{
    if(_class == warrior)
    {
        SetAsWarrior(gf);
    }
    if(_class == mage)
    {
        SetAsMage(gf);
    }
}

int CreatureClass::GetHealBonus()
{
    int k = executioningAbilities.size();
    int sum = 0;
    if(k)
        for(std::list<Ability*>::iterator i = executioningAbilities.begin(); i != executioningAbilities.end(); i++)
        {
            sum += (*i)->GetHealBonus();
        }
    return sum;
}
int CreatureClass::GetStrBonus()
{
    int k = executioningAbilities.size();
    int sum = 0;
    if(k)
        for(std::list<Ability*>::iterator i = executioningAbilities.begin(); i != executioningAbilities.end(); i++)
        {
            sum += (*i)->GetStrBonus();
        }
    return sum;
}
int CreatureClass::GetDexBonus()
{
    int k = executioningAbilities.size();
    int sum = 0;
    if(k)
        for(std::list<Ability*>::iterator i = executioningAbilities.begin(); i != executioningAbilities.end(); i++)
        {
            sum += (*i)->GetDexBonus();
        }
    return sum;
}
int CreatureClass::GetIntBonus()
{
    int k = executioningAbilities.size();
    int sum = 0;
    if(k)
        for(std::list<Ability*>::iterator i = executioningAbilities.begin(); i != executioningAbilities.end(); i++)
        {
            sum += (*i)->GetIntBonus();
        }
    return sum;
}

void CreatureClass::SetAsWarrior(GameField* gf)
{
    abilities.clear();
    AbilityBerserker* ab = new AbilityBerserker();
    abilities.push_back(ab);
    AbilityCrush* ac = new AbilityCrush(gf);
    abilities.push_back(ac);
}

void CreatureClass::SetAsMage(GameField* gf)
{
    abilities.clear();

    MagicDartAbility* ma = new MagicDartAbility();
    abilities.push_back(ma);
}
//возвращает число от 1 до 9
char CreatureClass::GetNumberFromKeyboard()
{
    char k;
    while(1)
    {
        k = getch();
        if( k =='1' ||
            k =='2' ||
            k =='3' ||
            k =='4' ||
            k =='5' ||
            k =='6' ||
            k =='7' ||
            k =='8' ||
            k =='9')
                return k;
    }
}

CreatureClass::~CreatureClass()
{
}