/*
 * File:   Ability.cc
 * Author: dvdking
 *
 * Created on April 30, 2012, 11:39 AM
 */

#include "Ability.h"
#include <stdio.h>
#include <string.h>
Ability::Ability()
{
    direction = 0;
}
/*
_|_|_|    _|    _|  _|_|_|    _|        _|_|_|    _|_|_|
_|    _|  _|    _|  _|    _|  _|          _|    _|
_|_|_|    _|    _|  _|_|_|    _|          _|    _|
_|        _|    _|  _|    _|  _|          _|    _|
_|          _|_|    _|_|_|    _|_|_|_|  _|_|_|    _|_|_|
 */

void Ability::Start(int str, int dex, int Int,int x, int y,  char dir, bool PrintMessages)
{
    direction = dir;
    _elapsed = 0;
    _ended = false;
}
void Ability::Update(bool PrintMessages)
{
    _elapsed++;
    if(_elapsed >= _duration)
    {
        _ended = true;
        _healBonus = 0;
        _strBonus = 0;
        _dexBonus = 0;
        _intBonus = 0;

        OnEnd(PrintMessages);
    }
    else
        OnUpdate(PrintMessages);
}

bool Ability::IsEnded()
{
    return _ended;
}

//char* Ability::GetCurrentMessage()
//{
//    return _currentMessage;
//    strcpy(_currentMessage, "");
//}

int Ability::GetHealBonus()
{
    return _healBonus;
}
int Ability::GetStrBonus()
{
    return _strBonus;
}
int Ability::GetDexBonus()
{
    return _dexBonus;
}
int Ability::GetIntBonus()
{
    return _intBonus;
}

int Ability::GetDamageGiven()
{
    return _damageGiven;
}

 /*
_|_|_|    _|_|_|      _|_|    _|_|_|_|_|  _|_|_|_|    _|_|_|  _|_|_|_|_|  _|_|_|_|  _|_|_|
_|    _|  _|    _|  _|    _|      _|      _|        _|            _|      _|        _|    _|
_|_|_|    _|_|_|    _|    _|      _|      _|_|_|    _|            _|      _|_|_|    _|    _|
_|        _|    _|  _|    _|      _|      _|        _|            _|      _|        _|    _|
_|        _|    _|    _|_|        _|      _|_|_|_|    _|_|_|      _|      _|_|_|_|  _|_|_|
 */

void Ability::OnEnd(bool PrintMessages){}

void Ability::OnUpdate(bool PrintMessages){}

Ability::~Ability()
{
    /*if(_currentMessage != NULL)
        delete[] _currentMessage;*/
}