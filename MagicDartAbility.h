/*
 * File:   MagicDart.h
 * Author: dvdking
 *
 * Created on May 1, 2012, 8:42 PM
 */

#ifndef MAGICDARTABILITY_H
#define	MAGICDARTABILITY_H
#include "Ability.h"
class MagicDartAbility:public Ability
{
public:
    MagicDartAbility();
    void Start(int str, int dex, int Int,int x, int y, char dir, bool PrintMessages);
    virtual ~MagicDartAbility();
private:
     int _crX;
     int _crY;
};

#endif	/* MAGICDART_H */