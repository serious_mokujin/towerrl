/*
 * File:   MagicDart.cpp
 * Author: dvdking
 *
 * Created on May 1, 2012, 8:50 PM
 */
#include "game.h"

#include "Creature.h"
#include "GameField.h"
MagicDart::MagicDart(int x, int y, char dir, int damage)
{
    _x = x;
    _y = y;
    _direction = dir;
    _damage = damage;
}

int MagicDart::Update()
{
    GetPositionInDirection(_direction, _x, _y);
    int w = field->GetWidth();

    if(_x >= w)
        _x = _x % w;
    if(_x < 0)
        _x = w + _x;
    if(_y >= field->GetHeight() || _y < 0) return - 1;
    if(field->GetTile(_x, _y) == '=')
    {
        return -1;
    }

    Creature* cr = GetCreature(_x, _y);
    if(cr != NULL)
    {
        cr->TakeDamage(_damage);
		if(!cr->Alive())
		{
			hero->GainExp(cr->GetLevel()*2);
			PrintMessage("You kill the monster!");
			KillCreature(cr);
		}
		return -1;
    }
	if(hero->GetX() == _x && hero->GetY() == _y)
	{
		PrintMessage("Your magic dart hit's you!");
		hero->TakeDamage(_damage);
		if(!hero->Alive())
		{
			PrintMessage("Congratilations, you kill yourself!");
		}
	}
}

void MagicDart::Print()
{
    move(_y, _x);
    printw("+");
}

void MagicDart::ShiftUp()
{
    _y++;
}

int MagicDart::GetDamage()
{
    return _damage;
}
int MagicDart::GetDirection()
{
    return _direction;
}
int MagicDart::GetX()
{
    return _x;
}
int MagicDart::GetY()
{
    return _y;
}

void MagicDart::GetPositionInDirection(char dir, int &x, int &y)
{
    switch(dir)
    {
        case '1':
            x--;
            y++;
            break;
        case '2':
            y++;
            break;
        case '3':
            x++;
            y++;
            break;
        case '4':
            x--;
            break;
        case '6':
            x++;
            break;
        case '7':
            x--;
            y--;
            break;
        case '8':
            y--;
            break;
        case '9':
            x++;
            y--;
            break;
    }
}

MagicDart::~MagicDart()
{
}