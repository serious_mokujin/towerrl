/* 
 * File:   game.h
 * Author: Semen
 *
 * Created on 20 Апрель 2012 г., 23:29
 */

#ifndef GAME_H
#define	GAME_H
#include "Creature.h"
#include "Chest.h"
#include "GameField.h"
#include "list"
#include "NameGenerator.h"
#include "MagicDart.h"

#define BUFFER_SIZZE 10
#define BUFFER_WIDTH 55

extern GameField *field; 
extern Creature *hero;
extern std::list<Creature*> creatures;
extern std::list<Chest*> chests;
extern std::list<MagicDart*> magicDarts;
extern int heightExplored;
extern char PlayerName[20];

extern int monstersKilled ;
extern int chestsOpened;
extern int damageTaken;
extern int damageGiven;
extern int healTaken;

static char **_mbuffer;


void PrintMessage(const char *message, bool printDate = true);
void InitializeGame();
void GenerateNewGame();
void DisposeGame();

void NewCreature(int x, int y, int lvl);

void NewChest(int x, int y);

void PrintCreatures();
void DeleteCreatures();
void DeleteChests();
void KillCreature(Creature *cr);

Creature* GetCreature(int x, int y);

void NewMagicDart(int x, int y, char dir,int damage);
void PrintsMagicDarts();
void UpdateMagicDarts();

void Shift();

void PrintGame();

void GameTurn();
int CalculateScore();
int rrandom(int min, int max);
static void shiftBuffer();
static void printBuffer();





#endif	/* GAME_H */

