/* 
 * File:   Chest.cc
 * Author: dvdking
 * 
 * Created on 21 Апрель 2012 г., 12:45
 */
#include "game.h"
#include "Chest.h"
#include <stdlib.h>

Chest::Chest(int X, int Y) 
{
    type = (Type)(rand() % 8);
    if(type >= 3)
        type = (Type)(rand() % 8);
    
    this->X = X;
    this->Y = Y;
}

void Chest::Open(Creature* creature)
{
    switch(type)
    {
        case HealChest:
            PrintMessage("You feel better."); 
            creature->Heal(5 + rand() % 5);
            break;
        case ExperienceChest:
            PrintMessage("You feel more experienced."); 
            creature->GainExp(1 + rand() % 5);
            break;
        case StrengthChest:
            creature->GainStr(1);
            PrintMessage("You feel stronger."); 
            break;
        case DexterityChest:
            creature->GainDex(1);
            PrintMessage("You feel more agile."); 
            break;
        case InteligenceChest:
            PrintMessage("You feel like a genius");
            creature->GainInt(1);
        case AngryChest:
            PrintMessage("Chest bites you! Arrh.. that's hurt!"); 
            creature->TakeDamage(1 + rand() % 4 + hero->GetLevel() * 2);       
            break;
        case UselessChest:
            PrintMessage("Chest disappears without any effect"); 
            break;
        case DegradationChest:
            PrintMessage("Your brain hurts and");
            PrintMessage("you can't remember a couple of things", false);
            creature->ReduceExp(2 + rand() % 5);
            break;
        default:
            PrintMessage("Something strange has happened"); 
            break;
    }
}

void Chest::Print()
{
    move(Y, X);
    printw("^");
}

Chest::~Chest() 
{
    
}

