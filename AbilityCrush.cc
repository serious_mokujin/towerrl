/*
 * File:   AbilityCrush.cc
 * Author: dvdking
 *
 * Created on May 1, 2012, 2:11 PM
 */

#include "AbilityCrush.h"
#include "game.h"
#include <string.h>
#include <stdio.h>
#include <list>
#include "Creature.h"
AbilityCrush::AbilityCrush(GameField* gf)
{
    this->gf = gf;
    direction = 1;
    _ended = true;
};

void AbilityCrush::Start(int str, int dex, int Int, int x, int y, char dir, bool PrintMessages)
{
    _crX = x;
    _crY = y;
    direction = dir;
    _elapsed = 0;
    _ended = false;
    _healBonus = 0;
    _strBonus = 0;
    _dexBonus = 0;
    _intBonus = 0;
    _duration = 6;
    _damageGiven = str * 5 + dex;
    switch(direction)
    {
        case '1':
            _crX--;
            _crY++;
            break;
        case '2':
            _crX++;
            break;
        case '3':
            _crX++;
            _crY++;
            break;
        case '4':
            _crX--;
            break;
        case '6':
            _crX++;
            break;
        case '7':
            _crX--;
            _crY--;
            break;
        case '8':
            _crY--;
            break;
        case '9':
            _crX++;
            _crY--;
            break;
    }
    cycleX(&x);
    if(gf->GetTile(_crX,_crY) == '=')
    {
        gf->DestroyTile(_crX, _crY);
        if(PrintMessages)
                PrintMessage("Wall destroys!");
    }
	std::list<Creature*>::iterator i = creatures.begin();
	while (i != creatures.end())
	{
		if((*i)->GetX() == _crX && (*i)->GetY() == _crY)
        {
            (*i)->TakeDamage(_damageGiven);

			if(PrintMessages)
                 PrintMessage("Monster takes strong strike to his head!");
        }

		if(!(*i)->Alive())
		{
			PrintMessage("You kill the monster!");
			delete (*i);
            i = creatures.erase(i);
		}
		else
		{
			++i;
		}
	}

    //if(PrintMessages)
      //   PrintMessage("You smash with all your power!");
}

void AbilityCrush::cycleX(int *x)
{
    int w = gf->GetWidth();

    if(*x >= w)
        *x = *x % w;
    if(*x < 0)
        *x = w + *x;
}

void AbilityCrush::OnEnd(bool PrintMessages)
{
}
AbilityCrush::~AbilityCrush() {
}