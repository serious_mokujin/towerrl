#include <curses.h>
#include "Creature.h"
#include "game.h"
#include "ScoreTable.h"
#include <string.h>
enum GS
{
    MAIN_MENU,
    INPUT_NAME,
    SET_CHAR_SET,
    CLASS_MENU,
    GAME,
    MORGUE,
    EXIT,
} gameState;

int menu(int count,...)
{
    int current = 0;
    va_list vlist;
    while(true)
    {
        va_start(vlist, count);
        clear();
        for(int i = 0; i < count; i++)
        {
            move(i, 0);
            if(i == current)
            {
                attroff(COLOR_PAIR(1));
                attron(COLOR_PAIR(2));
            }
            else
            {
                attroff(COLOR_PAIR(2));
                attron(COLOR_PAIR(1));
            }
            printw(va_arg(vlist, char *));
        }
        move(6,7);
        refresh();

        switch(getch())
        {
            case KEY_UP: current = current == 0 ? count - 1: current - 1;
                break;
            case KEY_DOWN: current = current == count - 1 ? 0: current + 1;
                break;
            case KEY_RIGHT:
            {
                clear();

                attroff(COLOR_PAIR(2));
                attroff(COLOR_PAIR(1));

                attron(COLOR_PAIR(3));
                return current;
            }
            case 'q': return 1;
        }
    }
    va_end(vlist);
}
int createCharacterMenu(Creature *creature)
{
    int n = 3;int i;

    int* stats = new int[n];
    char** names = new char*[n];

    stats[0] = 5;
    stats[1] = 5;
    stats[2] = 5;
    names[0] = "Strenght: ";
    names[1] = "Dexterity: ";
    names[2] = "intelligence: ";
    int leftStats = 5;
    int current = 0;

    while(true)
    {
        //*PRINT START
        clear();
        move(0,0);
        printw("Create new character");
        for(i = 0; i < n; i++)
        {
            move(i + 2, 0);
            if(i == current)
            {
                attroff(COLOR_PAIR(1));
                attron(COLOR_PAIR(2));
            }
            else
            {
                attroff(COLOR_PAIR(2));
                attron(COLOR_PAIR(1));
            }
            printw(names[i]);
            printw("%i", stats[i]);
        }

        attroff(COLOR_PAIR(2));
        attron(COLOR_PAIR(1));
        if(current == n)
        {
            attroff(COLOR_PAIR(1));
            attron(COLOR_PAIR(2));
        }
        move(n + 4, 0);
        printw("Complete");

        attroff(COLOR_PAIR(2));
        attron(COLOR_PAIR(1));

        move(n + 6, 0);
        printw("Stats left: ");
        printw("%i", leftStats);

        refresh();
        //*PRINT END

        switch(getch())
        {
            case KEY_UP: current = current == 0 ? n: current - 1;
                break;
            case KEY_DOWN: current = current == n ? 0: current + 1;
                break;
            case KEY_RIGHT:
                if(current == n)
                {
                    if(leftStats == 0)
                    {
                        creature->Reset();
                        creature->GainDex(stats[1] - 1);
                        creature->GainStr(stats[0] - 1);
                        creature->GainInt(stats[2] - 1);
                        attroff(COLOR_PAIR(1));
                        attroff(COLOR_PAIR(2));
                        attron(COLOR_PAIR(3));
						hero->Heal(hero->GetMaxHp());
                        return 0;
                    }
                    break;
                }
                if(leftStats != 0)
                {
                    stats[current] += 1;
                    leftStats--;
                }
                break;
            case KEY_LEFT:
                if(stats[current] != 0)
                {
                    stats[current] -= 1;
                    leftStats++;
                }
                break;
        }
    }
}

int InputPlayerNameMenu()
{
    noraw();
    echo();
    printw("Please introduce yourself: ");
    getnstr(PlayerName, 40);
    hero->SetName(PlayerName);
    raw();
    noecho();
	return 0;
}
int MorgueMenu()
{
#define countOnPage 10

    int curPage = 0;
    ScoreTable st;

    corpse cr;

    cr.score = CalculateScore();
    strcpy(cr.name, PlayerName);
    st.LoadTable();
    st.AddScore(cr);
    st.SaveTable();

    int corpseCount = st.GetCorpsesCount();

    while(true)
    {
        clear();

        move(0, 0);
        printw("Funny statistics: ");
        move(2, 0);
        printw("Enemies killed\t%i", monstersKilled);
        move(3, 0);
        printw("Damage taken\t%i", damageTaken);
        move(4, 0);
        printw("Damage given\t%i", damageGiven);
        move(5, 0);
        printw("Heal taken\t%i", healTaken);
        move(6, 0);
        printw("Chests opened\t%i", chestsOpened);
        move(7, 0);
        printw("Height reached\t%i%s", heightExplored * 2, " meters" );
        move(8, 0);
        printw("_______________________");
        attroff(COLOR_PAIR(1));
        attroff(COLOR_PAIR(2));
        attron(COLOR_PAIR(0));
        move(9, 0);
        printw("High scores: \n\n");
        st.PrintTable(curPage * countOnPage, countOnPage);

        attroff(COLOR_PAIR(1));
        attroff(COLOR_PAIR(2));
        attron(COLOR_PAIR(0));
        move(22,0);
        printw("\nUse arrows keys to move up and down in the table");
        printw("\nPress 'HOME' to go to the main menu");
        refresh();

        switch(getch())
        {
            case KEY_UP: curPage = curPage == 0 ? 0 : curPage - 1;
                break;
            case KEY_DOWN: curPage = curPage == corpseCount / countOnPage ? corpseCount / countOnPage : curPage + 1;
                break;
            case KEY_HOME:
                return 0;
        }
    }
}

void setupcurses()
{
    initscr();                          // Start curses mode
    raw();
    noecho();
    keypad(stdscr, TRUE);
    start_color();			/* Start color 			*/

    assume_default_colors(COLOR_WHITE, COLOR_BLACK);

    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_BLACK, COLOR_WHITE);

    init_pair(3, COLOR_WHITE, COLOR_BLACK);
}

int main(int argc, char** argv)
{
    setupcurses();
    InitializeGame();

    gameState = MAIN_MENU;

    int c;
    while(gameState != EXIT)
    {
        switch(gameState)
        {
            case MAIN_MENU:
                c = menu(2, "* Start new game", "* Exit");
                if(c == 0)
                {
                    gameState = INPUT_NAME;
                }
                else if(c == 1)
                    gameState = EXIT;
                break;
            case INPUT_NAME:
                InputPlayerNameMenu();
                GenerateNewGame();
                gameState = CLASS_MENU;
                break;
            case CLASS_MENU:
                c = menu(2, "* Warrior", "* Mage" );
                if(c == 0)
                {
                    hero->crClass.SetClass(warrior, field);
                }
                else if(c == 1)
                {
                    hero->crClass.SetClass(mage, field);
                }
                gameState = SET_CHAR_SET;
                break;
            case SET_CHAR_SET:
                createCharacterMenu(hero);

                PrintGame();
                PrintMessage("You are at the bottom of a tall tower.", true);
                PrintMessage("The only way out - to get to the top!", false);
                PrintMessage("Good luck!", false);
                gameState = GAME;
                break;

            case GAME:
                if(hero->Alive())
                    GameTurn();
                else
                {
                    flushinp();
                    PrintMessage("Press 'HOME' to go morgue");

                    while(getch() != KEY_HOME);
                    gameState = MORGUE;
                }
                break;
            case MORGUE:

                MorgueMenu();

                gameState = MAIN_MENU;
                break;
        }
    }

    DisposeGame();
    endwin();
    return 0;
}