/* 
 * File:   ScoreTable.h
 * Author: dvdking
 *
 * Created on 19 Апрель 2012 г., 20:08
 */
#include <list>
#ifndef SCORETABLE_H
#define	SCORETABLE_H

#define SC 10// scores' count
#define SCORES_PATH "morgue.mrg"

typedef struct
{
    char name[20];
    int score;
} corpse;

class ScoreTable 
{
public:
    int CurrentScore;
    
    ScoreTable();
    virtual ~ScoreTable();
    
    void LoadTable();
    void SaveTable();
    void AddScore(corpse crps);
    void PrintTable();
    void PrintTable(int start, int count);
    
    
    int GetCorpsesCount();

private:
    corpse lastCorpse;
    std::list<corpse> corpses;
    
};

#endif	/* SCORETABLE_H */

