/* 
 * File:   CreatureClass.h
 * Author: dvdking
 *
 * Created on April 30, 2012, 11:26 AM
 */
#ifndef CREATURECLASS_H
#define	CREATURECLASS_H
//#include "Creature.h"
#include "Ability.h"
#include <list>
#include "GameField.h"



enum Class
{
  warrior,
  mage,
  chief
};

class CreatureClass{
public:
    CreatureClass();
    
    void UpdateAbilities(bool PrintMessages);
    
    int UseSpecialAbility(int str, int dex, int Int,int x, int y,bool PrintMessages);
    void SetClass(Class _class, GameField* gf);
    
    int GetHealBonus();
    int GetStrBonus();
    int GetDexBonus();
    int GetIntBonus(); 

    
    virtual ~CreatureClass();
private:
    std::list<Ability*> abilities;
    std::list<Ability*> executioningAbilities;
    char GetNumberFromKeyboard();
    
    void SetAsWarrior(GameField* gf);
    void SetAsChief(GameField* gf);
    void SetAsMage(GameField* gf);
};

#endif	/* CREATURECLASS_H */

