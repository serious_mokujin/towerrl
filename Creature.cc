/*
 * File:   Creature.cc
 * Author: Semen
 *
 * Created on 16 Апрель 2012 г., 10:14
 */

#include "Creature.h"
#include <curses.h>
#include <stdlib.h>
#include "game.h"
#include "Chest.h"
#include <string.h>

Creature::Creature(GameField *field)
{
    _field = field;
    _hero = false;
    Reset();
}
void Creature::Reset()
{
   // SetPos(0,0);

    _strength = 1;
    _dexterity = 1;
    _intelligence = 1;
    _expierence = 0;
    _maxexpierence = 1;
    _level = 0;
    _hitpoints = 0;
    _fallingHeight = 0;
    _jumpStage = 0;
    _jumpStageCounter = 0;
    _jumpLeft = false;
    _potions = 3;
	_regenerationCounter = 0;

    GainLevel();
    _hitpoints = GetMaxHp();
    _jumped = false;
    _fall = false;
    _caught = false;
    _alive = true;

    _moveRight = false;
    _moveDown = false;
    _moveUp = false;
}
void Creature::MakeMeAHero()
{
    _hero = true;
}
int Creature::GetX()
{
    return _x;
}
int Creature::GetY()
{
    return _y;
}
void Creature::SetX(int x)
{
    _x = x;
}
void Creature::SetY(int y)
{
    _y = y;
}
void Creature::SetClass(Class _class)
{
   // this->_class.SetClass(_class, this);
}
void Creature::_getDown()
{
    if(_fall)
    {
        if(_fallingHeight > 3)
        {
            TakeDamage(_fallingHeight);
            if(_hero)
            {
                char b[35];
                sprintf(b,"Hurt! Fall from %d meters, -%d hp",_fallingHeight * 2, _fallingHeight);
                PrintMessage(b);
            }
        }
        _fall = false;
        _jumped = false;
        _fallingHeight = 0;
    }
}

bool Creature::fallDown()
{
    if((_get(4) == '=' && _get(7) != '=') || (_get(6) == '=' && _get(9) != '='))
    {
        _caught = true;
        _jumped = false;
        _getDown();
    }
    if(_fall)
    {
        if(_jumped)
        {
            if(_jumpStage == _jumpStageCounter)
            {
                if(_jumpLeft)
                {
                    if(_get(1) != '=')
                        displace(1);
                    else if(_get(4) != '=')
                        displace(4);
                    else
                    {
                        displace(2);
                        _jumped = false;
                    }
                }
                else
                {
                    if(_get(3) != '=')
                        displace(3);
                    else if(_get(6) != '=')
                        displace(6);
                    else
                    {
                        displace(2);
                        _jumped = false;
                    }
                }
                _jumpStageCounter = 1;
                _jumpStage++;
            }
            else
            {
                _jumpStageCounter++;
                displace(2);
            }
        }
        else
            displace(2);
        _fallingHeight++;
        return true;
    }
    return false;
}

bool Creature::Update()
{
    bool turn = false;
    if(_alive)
    {
        if(!_caught)
        {
            char c = _get(2);
            if(c != '=' && _get(5) != '#' && c != '#' && c != '^' && (c < 97 || c > 122) && c != '@')
                _fall = true;
            else
                _getDown();
        }
        if(_fall)
            turn = fallDown();
        if(!_hero && !turn)
        {
            aiturn();
            turn = true;
        }
        if(_regenerationCounter >= 6)
        {
            Heal(1);
            _regenerationCounter = 0;
        }
        else
            _regenerationCounter++;

        checkAlive();
    }
    return turn;
}

void Creature::aiturn()
{
    los* line = new los();
    if(GetLos(hero->GetX(), hero->GetY(), line)) // герой видим
    {
        if(hero->GetY() > _y)
        {
            _moveUp = false;
            _moveDown = true;
        }
        else if(hero->GetY() < _y)
        {
            _moveUp = true;
            _moveDown = false;
        }
        else
        {
            _moveUp = false;
            _moveDown = false;
            if(hero->GetX() > _x)
                _moveRight = true;
            else
                _moveRight = false;
        }
    }
        bool result = false;

        for(int i = 0; !result; i++)
        {
            if(_moveRight)
            {
                if(_moveDown)
                {
                    if(_get(2) != '=')
                        result = Move(2);
                    else if(_get(3) != '=')
                        result = Move(3);
                    else if(( _get(6) != '=') && ( _get(3) == '=' || _get(3) == '#' ))
                        result = Move(6);
                }
                else if(_moveUp)
                {
                    if((_get(8) != '=') &&
                            (   (_get(7) == '=') ||
                                (_get(9) == '=') ||
                                (_get(8) == '#') ||
                                (_get(8) == '@')))
                        result = Move(8);
                    else if((_get(9) != '=') &&
                            (   (_get(8) == '=') ||
                                (_get(9) == '#') ||
                                (_get(9) == '@') ||
                               ((_get(9) == '.') && ((_get(6) == '=') || (_get(6) == '#'))) ) )
                        result = Move(9);
                    else if(( _get(6) != '=') && ( _get(3) == '=' || _get(3) == '#' ))
                        result = Move(6);
                }
                else
                    if(( _get(6) != '=') && ( _get(3) == '=' || _get(3) == '#' ))
                        result = Move(6);
            }
            else
            {
                if(_moveDown)
                {
                    if(_get(2) != '=')
                        result = Move(2);
                    else if(_get(1) != '=')
                        result = Move(1);
                    else if(( _get(4) != '=')&& ( _get(1) == '=' || _get(1) == '#' ))
                        result = Move(4);
                }
                else if(_moveUp)
                {
                    if((_get(8) != '=') &&
                            (   (_get(7) == '=') ||
                                (_get(9) == '=') ||
                                (_get(8) == '#') ||
                                (_get(8) == '@')))
                        result = Move(8);
                    else if((_get(7) != '=') &&
                            (   (_get(8) == '=') ||
                                (_get(7) == '#') ||
                                (_get(7) == '@')||
                               ((_get(7) == '.') && ((_get(4) == '=') || (_get(4) == '#'))) ) )
                        result = Move(7);
                    else if(( _get(4) != '=')&& ( _get(1) == '=' || _get(1) == '#' ))
                        result = Move(4);
                }
                else
                    if(( _get(4) != '=')&& ( _get(1) == '=' || _get(1) == '#' ))
                        result = Move(4);
            }

            if(!result)
            {
                if(i == 0)
                    _moveRight = !_moveRight;
                else
                {
                    Move(5);
                    break;
                }
            }
        }
    //}
//    else
//    {
//        if(rand() % 2 && ( _get(6, 0) != '=')&& ( _get(3, 0) == '=' || _get(3, 0) == '#' ))
//            Move(6);
//        else if(( _get(4, 0) != '=')&& ( _get(1, 0) == '=' || _get(1, 0) == '#' ))
//            Move(4);
//        else
//            Move(5);
//    }
    delete line;
}
// Алгоритм Брезенхэма для рисования линий
los Creature::BuildLine(int x2, int y2)
{
    los vec;
    int x1 = _x;
    int y1 = _y;
    const int deltaX = abs(x2 - x1);
    const int deltaY = abs(y2 - y1);
    const int signX = x1 < x2 ? 1 : -1;
    const int signY = y1 < y2 ? 1 : -1;
    //
    int error = deltaX - deltaY;
    //
	point p;
	p.x = x2;
	p.y = y2;
    vec.push_back(p);// setPixel(x2, y2);
    while(x1 != x2 || y1 != y2)
    {
		p.x = x1;
		p.y = y1;
        vec.push_back(p); // setPixel(x1, y1);
        const int error2 = error * 2;
        //
        if(error2 > -deltaY)
        {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX)
        {
            error += deltaX;
            y1 += signY;
        }
    }
    return vec;
}
bool Creature::LOSisVisible(los *line)
{
    int cx, cy;
    for(los::iterator i = line->begin(); i != line->end(); ++i)
    {
        cx = (*i).x;
        cy = (*i).y;
        cycleX(&cx);
        if(field->GetTile(cx, cy) == '=')
            return false;
    }
    return true;
}
void swap(los *l1, los *l2)
{
    los tmp = *l1;
    *l1 = *l2;
    *l2 = tmp;
}
bool Creature::Visible(int x, int y)
{
    los ln1 = BuildLine(x - field->GetWidth(), y);
    los ln2 = BuildLine(x, y);
    los ln3 = BuildLine(x + field->GetWidth(), y);

    if (ln1.size() > ln2.size()) swap(&ln1, &ln2);
    if (ln2.size() > ln3.size()) swap(&ln2, &ln3);
    if (ln1.size() > ln2.size()) swap(&ln1, &ln2);

    if(LOSisVisible(&ln1))
    {
        _printLOS(&ln1);
        return true;
    }
    else if(LOSisVisible(&ln2))
    {
        _printLOS(&ln2);
        return true;
    }
    else if(LOSisVisible(&ln3))
    {
        _printLOS(&ln3);
        return true;
    }
    return false;
}
bool Creature::GetLos(int x, int y, los *line)
{
    los ln1 = BuildLine(x - field->GetWidth(), y);
    los ln2 = BuildLine(x, y);
    los ln3 = BuildLine(x + field->GetWidth(), y);

    if (ln1.size() > ln2.size()) swap(&ln1, &ln2);
    if (ln2.size() > ln3.size()) swap(&ln2, &ln3);
    if (ln1.size() > ln2.size()) swap(&ln1, &ln2);

    if(LOSisVisible(&ln1))
    {
        *line = ln1;
        return true;
    }
    else if(LOSisVisible(&ln2))
    {
        *line = ln2;
        return true;
    }
    else if(LOSisVisible(&ln3))
    {
        *line = ln3;
        return true;
    }
    return false;
}
void Creature::_printLOS(los *line)
{
    int cx, cy;
    if(line->size() > 1)
        for(los::iterator i = line->begin()+1; i != (line->end()); ++i)
        {
            cx = (*i).x;
            cy = (*i).y;
            cycleX(&cx);
            move(cy, cx);
            printw("*");
        }
    refresh();
}
bool Creature::Alive()
{
    return _alive;
}
bool Creature::Caught()
{
    return _caught;
}
bool Creature::Falling()
{
    return _fall;
}
void Creature::saySmth(char theme, char* name)
{
    char b[35];
    switch(theme)
    {
        case 'a':
            switch(rand()%5)
            {
                case 0:
                    sprintf(b, "%s: I'll kill you, %s!!",name, hero->GetName());
                    break;
                case 1:
                    sprintf(b, "%s: I'll find you anywhere!!",name, hero->GetName());
                    break;
                case 2:
                    sprintf(b, "%s: Not so easy!",name);
                    break;
                case 3:
                    sprintf(b, "%s: My Precious... ",name);
                    break;
                case 4:
                    sprintf(b, "%s: FUS RO DAAAH!!!!!! ",name);
                    break;
            }
            break;
        case 'd':
            switch(rand()%3)
            {
                case 0:
                    sprintf(b, "%s: See you in hell, %s!!",name, hero->GetName());
                    break;
                case 1:
                    sprintf(b, "%s: My wife, my children...",name, hero->GetName());
                    break;
                case 2:
                    sprintf(b, "%s: Holly angels!",name, hero->GetName());
                    break;
            }
            break;
        case 'w':
            switch(rand()%3)
            {
                case 0:
                    sprintf(b, "%s: Ha, yet another stupid looser!",name, hero->GetName());
                    break;
                case 1:
                    sprintf(b, "%s: You remember the great ", name);
                    break;
                case 2:
                    sprintf(b, "%s: Burn in hell, ",name, hero->GetName());
                    break;
            }
            break;
    }
    PrintMessage(b);
}
bool Creature::Move(unsigned short int dir)
{
    if(dir != 5)
        _caught = false;

    switch(_get(dir))
    {
        case '=':
            return false;
        case '.':
        case '^':
        case '#':
            displace(dir);
            if((dir == 7 || dir == 9) && _get(2) == '.')
            {
                _jumped = true;
                _jumpStage = 1;
                _jumpLeft = dir == 7;
                _jumpStageCounter = 1;
            }
            return true;

        case '@':
            if(!_hero)
            {
                if(rand()%3 == 1)
                    saySmth('a', _name);
                if(rrandom(0, 1 + hero->GetDexterity()) < hero->GetDexterity() / 2)
                    PrintMessage("You dodged from monster's impact!");
                else
                {
                    if(rrandom(0, 1 + this->GetDexterity()) == 0)
                        PrintMessage("Monster missed you!");
                    else
                    {
                        hero->TakeDamage(CalculateDamage(hero));
                        PrintMessage("Monster hits you.");
                        if(hero->GetHp() <= 0)
                            if(rand()%3 == 1)
                                saySmth('w', _name);
                    }
                }
            }
            return true;
        default:
            if((_get(dir) > 96 && _get(dir) < 123))
            {
                if(_hero)
                {
                    Creature* cr = _getCr(dir);
                    if(cr != NULL)
                    {
                        if(rrandom(0, 1 + cr->_dexterity) < cr->_dexterity / 2)
                            PrintMessage("Your target dodged from impact");
                        else
                        {
                            if(rrandom(0, 1 + this->_dexterity) == 0)
                                PrintMessage("You missed the target");
                            else
                            {
                                int d = CalculateDamage(cr);
                                cr->TakeDamage(d);

                                int hp = (cr->_hitpoints * 100)/cr->GetMaxHp();
                                if(hp>=75)
                                    PrintMessage("You hit monster. He's lightly wounded");
                                else if(hp>=50)
                                    PrintMessage("You hit monster. He's moderately wounded");
                                else if(hp>=25)
                                    PrintMessage("You hit monster. He's seriously wounded");
                                else
                                    PrintMessage("You hit monster. He's almost dead");
                            }
                        }
                    }
                    cr->checkAlive();
                    if(!cr->_alive)
                    {
                        if(rand()%3 == 1)
                            saySmth('d', cr->GetName());
                        PrintMessage("You kill the monster!");
                        GainExp(cr->GetLevel()*2);
                        KillCreature(cr);
                    }
                }
                return true;
            }
    }
    return false;
}
void Creature::SetName(char* name)
{
    /*if(_name != NULL)
        delete _name;*/
    _name = name;
}
char* Creature::GetName()
{
    return _name;
}
void Creature::QuaffPotion()
{
    if(_potions > 0)
    {
        this->Heal(GetMaxHp() / (2 + rand() % 2));
        _potions--;
        PrintMessage("You feel refreshed");
        char b[35];
        if(_potions > 1)
        {
            sprintf(b,"%d potions left",_potions);
        }
        else
        {
            sprintf(b,"%d potion left",_potions);
        }
        PrintMessage(b);
    }
    else
    {
        if(rand() % 2)
            PrintMessage("Unfortunately you don't carry any potions");
        else
            PrintMessage("There are no potions in your inventory");
    }
}
int Creature::GetDexterity()
{
    return _dexterity + crClass.GetDexBonus();
}
int Creature::GetStrength()
{
    return _strength + crClass.GetStrBonus();
}
int Creature::GetIntelegence()
{
    return _intelligence + crClass.GetIntBonus();
}
int Creature::GetExp(){return _expierence;}
int Creature::GetMaxHp()
{
    return 10 + _level*2+GetStrength()*3;;
}
int Creature::GetHp(){return _hitpoints;}
int Creature::GetMaxExp(){return _maxexpierence;}
void Creature::cycleX(int *x)
{
    int w = _field->GetWidth();

    if(*x >= w)
        *x = *x % w;
    if(*x < 0)
        *x = w + *x;
}
void Creature::cycleY(int *y)
{
    int h = _field->GetHeight();

    if(*y >= h)
        *y = *y % h;
    if(*y < 0)
        *y = h + *y;
}
void Creature::SetPos(int x, int y)
{
    _x = x;
    _y = y;
}
void Creature::GetPositionInDirection(int dir, int &x, int &y)
{
    switch(dir)
    {
        case 1:
            x--;
            y++;
            break;
        case 2:
            y++;
            break;
        case 3:
            x++;
            y++;
            break;
        case 4:
            x--;
            break;
        case 6:
            x++;
            break;
        case 7:
            x--;
            y--;
            break;
        case 8:
            y--;
            break;
        case 9:
            x++;
            y--;
            break;
    }
}
void Creature::displace(unsigned short int dir)
{
    GetPositionInDirection(dir, _x, _y);
    cycleX(&_x);
}
void Creature::checkAlive()
{
    if(_y >= _field->GetHeight() || _y < 0)
        _alive = false;
    if(_hitpoints <= 0)
        _alive = false;
    if(_hero && !_alive)
        PrintMessage("You die...");
}

Creature* Creature::_getCr(unsigned short int dir)
{
    int x = _x, y = _y;
    GetPositionInDirection(dir, x, y);
    cycleX(&x);
    cycleY(&y);
    return GetCreature(x, y);
}
char Creature::_get(unsigned short int dir)
{
    int x = _x, y = _y;
    GetPositionInDirection(dir, x, y);

    if(y >= _field->GetHeight() || y < 0)
        return ' ';
    else
    {
        cycleX(&x);
        return mvinch(y, x) & A_CHARTEXT;
    }
}
void Creature::GainLevel()
{
    while(_expierence >= _maxexpierence)
    {
        _level += 1;
        _expierence -= _maxexpierence;

        if(_expierence < 0) _expierence = 0;

        _maxexpierence = 15 + _maxexpierence * 1.5;

        if(this->_level != 1)
        {
            if(_hero)
            {
                char b[35];
                sprintf(b, "Level %d gained! Choose your stat:", _level);
                PrintMessage(b);
                PrintMessage("(D)exterity, (S)trength or (I)ntelligence?");
                while(1)
                {
                    switch(getch())
                    {
                        case 's':
                        case 'S':
                            _strength++;
                            PrintMessage("You feel stronger.");
                            goto m;
                        case 'd':
                        case 'D':
                            _dexterity++;
                            PrintMessage("You feel more agile.");
                            goto m;
						case 'i':
                        case 'I':
							_intelligence++;
                            PrintMessage("You feel clever.");
                            goto m;
                    }
                }
            }
            else
            {
                if(rand()%2)
                    _strength++;
                else if(rand() % 2)
                    _dexterity++;
				else
					_intelligence++;
            }
        }
        m:;
    }
}
void Creature::GainLevel(int lvl)
{
    while(_level < lvl)
    {
        _expierence = _maxexpierence;
        GainLevel();
    }
}
void Creature::ReduceLevel()
{
    _level -= 1;
    _maxexpierence = _maxexpierence / 1.5 - 15;
    _expierence += _maxexpierence;

    if(rand()%2)
        _strength--;
    else
        _dexterity--;
}

void Creature::GainExp(int amount)
{
    _expierence += amount;

    if(_expierence >= _maxexpierence)
        GainLevel();
}
void Creature::ReduceExp(int amount)
{
    _expierence -= amount;

    if(_expierence <= 0)
        ReduceLevel();
}
void Creature::GainStr(int amount)
{
    _strength += amount;
}
void Creature::GainDex(int amount)
{
    _dexterity += amount;
}
void Creature::GainInt(int amount)
{
    _intelligence += amount;
}
void Creature::TakeDamage(int amount)
{
    _hitpoints -= amount;

    if(_hero)
        damageTaken += amount;
    else
        damageGiven += amount;
    checkAlive();
	if(!_alive && !_hero) monstersKilled++;
}
void Creature::Heal(int amount)
{
    if(_hero) healTaken += amount;
    _hitpoints += amount;
    if(_hitpoints > GetMaxHp())
        _hitpoints = GetMaxHp();
}
int Creature::CalculateDamage(Creature* cr)
{
    int strFactor = rrandom(this->GetStrength() / 2, 1 + this->GetStrength());
    int dexFactor = rrandom(0, 1 + this->GetDexterity());
    int defFac = cr->GetStrength() / 10;

    if(defFac == 0) defFac = 1;

    return (strFactor + dexFactor) / defFac;
}
int Creature::GetLevel()
{
    return _level;
}
void Creature::Print()
{
    move(_y, _x);
    printw("%c", _hero ? '@' : 96 + _level);
    if(_hero)
        PrintInfo();
}
void Creature::PrintInfo()
{
    double i;
    int w = _field->GetWidth();
    move(0, w + 2);
    printw("HP:\t%d/%d\t",_hitpoints,GetMaxHp());
    if(_hitpoints>0)
    for(i = 0; i < GetMaxHp() ; i += (double)(GetMaxHp() / 15.0))
        printw(_hitpoints>i ? "!" : ".");

    move(1, w + 2);
    printw("EXP:\t%d/%d\t",_expierence,_maxexpierence);
    move(2, w + 2);
    printw("LVL:\t%d",_level);
    move(3, w + 2);
    printw("STR:\t%d",GetStrength());
    move(4, w + 2);
    printw("DEX:\t%d",GetDexterity());
    move(5, w + 2);
    printw("INT:\t%d",GetIntelegence());
}
Creature::~Creature()
{
}