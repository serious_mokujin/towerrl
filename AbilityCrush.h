/* 
 * File:   AbilityCrush.h
 * Author: dvdking
 *
 * Created on May 1, 2012, 2:11 PM
 */

#ifndef ABILITYCRUSH_H
#define	ABILITYCRUSH_H
#include "Ability.h"
#include "GameField.h"
class AbilityCrush: public Ability {
public:
    static const bool directionNeeded = true;
    
    AbilityCrush(GameField* gf);
    
    void Start(int str, int dex, int Int,int x, int y, char dir, bool PrintMessages);
    
    virtual ~AbilityCrush();
private:
     GameField* gf;
     
     int _crX;
     int _crY;
     void cycleX(int *x);
     void OnEnd(bool PrintMessages);
};

#endif	/* ABILITYCRUSH_H */

