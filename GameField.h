/* 
 * File:   GameField.h
 * Author: Semen
 *
 * Created on 15 Апрель 2012 г., 21:17
 */

#ifndef GAMEFIELD_H

#define	GAMEFIELD_H

class GameField {
public:
    GameField(int width, int height);
    void Print();
    void ShiftUp();
    void ScrollLeft();
    void ScrollRight();
    char GetTile(int x, int y);
    void DestroyTile(int x, int y);
    int GetWidth();
    int GetHeight();
    void StartGeneration();
    virtual ~GameField();
private:
    int _width;
    int _height;
    char **_field;
    int _scroll;
    
    void GenerateLine(short line, bool firstGen);
};

#endif	/* GAMEFIELD_H */

